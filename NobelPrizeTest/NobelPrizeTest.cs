﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NobelPrizeWPF;
using NobelPrizeWPF.Model;

namespace NobelPrizeTest
{
    [TestClass]
    public class NobelPrizeTest
    {
        [TestMethod]
        public void IsValid_Valid_True()
        {
            NobelPrize target = new NobelPrize();
            Achievement achievement = new Achievement(1, 2100, new Category(1, "Область науки"), "Описание");
            target.NewAchievement(achievement);
            Laureate laureate = new Laureate(1, "Иван Иванов", new Country(1, "Страна"));
            target.NewLaureate(laureate);
            Assert.IsTrue(target.IsValid, "Экземпляр действителен");
        }

        [TestMethod]
        public void IsValid_NullAchievement_True()
        {
            NobelPrize target = new NobelPrize();
            Achievement achievement = new Achievement(1, 2100, new Category(1, "Область науки"), "Описание");
            target.NewAchievement(achievement);
            Assert.IsFalse(target.IsValid, "Экземпляр недействителен");
        }

        [TestMethod]
        public void IsValid_NullLaureate_True()
        {
            NobelPrize target = new NobelPrize();
            Laureate laureate = new Laureate(1, "Иван Иванов", new Country(1, "Страна"));
            target.NewLaureate(laureate);
            Assert.IsFalse(target.IsValid, "Экземпляр недействителен");
        }
    }
}
