using Microsoft.VisualStudio.TestTools.UnitTesting;
using NobelPrizeWPF.Model;

namespace NobelPrizeTest
{
    [TestClass]
    public class OtherTest
    {
        [TestMethod]
        public void IsNotNullCategory_Valid_True()
        {
            Category target = new Category(1, "������� �����");
            Assert.IsNotNull(target, "��������� ������������");
        }

        [TestMethod]
        public void IsNotNullCountry_Valid_True()
        {
            Country target = new Country(1, "������");
            Assert.IsNotNull(target, "��������� ������������");
        }

        [TestMethod]
        public void IsNotNullStatisticInfo_Valid_True()
        {
            Statistic target = new Statistic("�����", 1);
            Assert.IsNotNull(target, "��������� ������������");
        }
    }
}
