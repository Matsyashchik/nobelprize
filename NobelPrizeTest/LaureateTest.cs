using Microsoft.VisualStudio.TestTools.UnitTesting;
using NobelPrizeWPF.Model;

namespace NobelPrizeTest
{
    [TestClass]
    public class LaureateTest
    {
        [TestMethod]
        public void IsValid_Valid_True()
        {
            Laureate target = new Laureate();
            target.NewName("���� ������");
            target.NewCountry(new Country(1, "������"));
            Assert.IsTrue(target.IsValid, "��������� ������������");
        }

        [TestMethod]
        public void IsValid_EmptyName_False()
        {
            Laureate target = new Laureate();
            target.NewName(string.Empty);
            target.NewCountry(new Country(1, "������"));
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_NullName_False()
        {
            Laureate target = new Laureate();
            target.NewName(null);
            target.NewCountry(new Country(1, "������"));
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_WhiteSpaceName_False()
        {
            Laureate target = new Laureate();
            target.NewName("   ");
            target.NewCountry(new Country(1, "������"));
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_NullCountry_False()
        {
            Laureate target = new Laureate();
            target.NewName("���� ������");
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }
    }
}
