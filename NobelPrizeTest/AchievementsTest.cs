using Microsoft.VisualStudio.TestTools.UnitTesting;
using NobelPrizeWPF.Model;

namespace NobelPrizeTest
{
    [TestClass]
    public class AchievementsTest
    {
        [TestMethod]
        public void IsValid_Valid_True()
        {
            Achievement target = new Achievement();
            target.NewYear(2000);
            target.NewCategory(new Category(1, "������� �����"));
            target.NewDescription("��������");
            Assert.IsTrue(target.IsValid, "��������� ������������");
        }

        [TestMethod]
        public void IsValid_YearLess_False()
        {
            Achievement target = new Achievement();
            target.NewYear(1900);
            target.NewCategory(new Category(1, "������� �����"));
            target.NewDescription("��������");
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_YearMore_False()
        {
            Achievement target = new Achievement();
            target.NewYear(2100);
            target.NewCategory(new Category(1, "������� �����"));
            target.NewDescription("��������");
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_NullCategory_False()
        {
            Achievement target = new Achievement();
            target.NewYear(2000);
            target.NewDescription("��������");
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_EmptyDescription_False()
        {
            Achievement target = new Achievement();
            target.NewYear(2000);
            target.NewCategory(new Category(1, "������� �����"));
            target.NewDescription(string.Empty);
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_WhiteSpaceDescription_False()
        {
            Achievement target = new Achievement();
            target.NewYear(2000);
            target.NewCategory(new Category(1, "������� �����"));
            target.NewDescription("    ");
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_NullDescription_False()
        {
            Achievement target = new Achievement();
            target.NewYear(2000);
            target.NewCategory(new Category(1, "������� �����"));
            target.NewDescription(null);
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }

        [TestMethod]
        public void IsValid_LongDescription_False()
        {
            Achievement target = new Achievement();
            target.NewYear(2000);
            target.NewCategory(new Category(1, "������� �����"));
            target.NewDescription("����� ������� ��������......................" +
                ".............................................................. " +
                "..............................................................");
            Assert.IsFalse(target.IsValid, "��������� ��������������");
        }
    }
}
