﻿using NobelPrizeWPF.DataAccess;
using NobelPrizeWPF.Model;
using NobelPrizeWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;

namespace NobelPrizeWPF
{
    class MainWindowViewModel : WorkspaceViewModel
    {
        #region Поля

        ReadOnlyCollection<CommandViewModel> _commands;
        readonly DatabaseWorker database;
        readonly Workspace workspace;

        #endregion // Fields

        public MainWindowViewModel(string connection)
        {
            base.DisplayName = "Нобелевская премия";
            database = new DatabaseWorker(connection);
            database.ErrorMessage += OnErrorToRepository;
            workspace = new Workspace();
            workspace.WorkspaceEvent += OnWorkspaceToRepository;
        }

        #region Комманды управления
        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                if (_commands == null)
                {
                    List<CommandViewModel> cmds = this.CreateCommands();
                    _commands = new ReadOnlyCollection<CommandViewModel>(cmds);
                }
                return _commands;
            }
        }

        private List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>
            {
                new CommandViewModel("Показать все Нобелевские премии", new RelayCommand(param => ShowAllNobelPrizes())),
                new CommandViewModel("Добавить нобелевскую премию", new RelayCommand(param => AddNobelPrize())),
                new CommandViewModel("Показать все открытия", new RelayCommand(param => ShowAllAchievements())),
                new CommandViewModel("Добавить открытие", new RelayCommand(param => AddAchievement())),
                new CommandViewModel("Показать всех лауреатов", new RelayCommand(param => ShowAllLaureates())),
                new CommandViewModel("Добавить лауреата", new RelayCommand(param => AddLaureate())),
                new CommandViewModel("Показать статистику", new RelayCommand(param => ShowStatistic())),
            };
        }
        
        private void ShowAllAchievements()
        {
            AllAchievementsViewModel allAchievementsWorkspace = this.Workspaces.FirstOrDefault(vm => vm is AllAchievementsViewModel) as AllAchievementsViewModel;
            if (allAchievementsWorkspace == null)
            {
                allAchievementsWorkspace = new AllAchievementsViewModel(workspace, database);
                Workspaces.Add(allAchievementsWorkspace);
            }
            this.SetActiveWorkspace(allAchievementsWorkspace);
        }

        private void AddAchievement()
        {
            Achievement achievement = new Achievement();
            AchievementViewModel achievementViewModel = new AchievementViewModel(achievement, database);
            Workspaces.Add(achievementViewModel);
            this.SetActiveWorkspace(achievementViewModel);
        }

        private void ShowAllLaureates()
        {
            AllLaureatesViewModel allLaureatesWorkspace = this.Workspaces.FirstOrDefault(vm => vm is AllLaureatesViewModel) as AllLaureatesViewModel;
            if (allLaureatesWorkspace == null)
            {
                allLaureatesWorkspace = new AllLaureatesViewModel(workspace, database);
                Workspaces.Add(allLaureatesWorkspace);
            }
            this.SetActiveWorkspace(allLaureatesWorkspace);
        }

        private void AddLaureate()
        {
            Laureate laureate = new Laureate();
            LaureateViewModel laureateViewModel = new LaureateViewModel(laureate, database);
            Workspaces.Add(laureateViewModel);
            this.SetActiveWorkspace(laureateViewModel);
        }

        private void ShowAllNobelPrizes()
        {
            AllNobelPrizesViewModel allNobelPrizesWorkspace = this.Workspaces.FirstOrDefault(vm => vm is AllNobelPrizesViewModel) as AllNobelPrizesViewModel;
            if (allNobelPrizesWorkspace == null)
            {
                allNobelPrizesWorkspace = new AllNobelPrizesViewModel(workspace, database);
                Workspaces.Add(allNobelPrizesWorkspace);
            }
            this.SetActiveWorkspace(allNobelPrizesWorkspace);
        }

        private void AddNobelPrize()
        {
            NobelPrize nobelPrize = new NobelPrize();
            NobelPrizeViewModel nobelPrizeViewModel = new NobelPrizeViewModel(nobelPrize, database);
            Workspaces.Add(nobelPrizeViewModel);
            this.SetActiveWorkspace(nobelPrizeViewModel);
        }

        private void ShowStatistic()
        {
            StatisticViewModel statisticWorkspace = this.Workspaces.FirstOrDefault(vm => vm is StatisticViewModel) as StatisticViewModel;
            if (statisticWorkspace == null)
            {
                statisticWorkspace = new StatisticViewModel(database);
                Workspaces.Add(statisticWorkspace);
            }
            this.SetActiveWorkspace(statisticWorkspace);
        }

        #endregion

        #region Рабочее пространство
        public ObservableCollection<WorkspaceViewModel> Workspaces
        {
            get
            {
                if (workspace.workspaces == null)
                {
                    workspace.workspaces = new ObservableCollection<WorkspaceViewModel>();
                    workspace.workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }
                return workspace.workspaces;
            }
        }
        #endregion

        #region Обработчики событий
        void OnErrorToRepository(object sender, ErrorEventArgs e)
        {
            ErrorConnection = e.message;
        }

        private void OnWorkspaceToRepository(object sender, WorkspaceEventArgs e)
        {
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(Workspaces);
            if (collectionView != null)
            {
                collectionView.MoveCurrentTo(e.workspaceViewModel);
            }
        }

        void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.NewItems)
                    workspace.RequestClose += this.OnWorkspaceRequestClose;

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.OldItems)
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            workspace.Dispose();
            this.Workspaces.Remove(workspace);
        }

        #region Закрытые методы

        string errorConnection;
        public string ErrorConnection
        {
            get { return errorConnection; }
            set
            {
                errorConnection = value;
                base.OnPropertyChanged("ErrorConnection");
            }
        }
        #endregion

        void SetActiveWorkspace(WorkspaceViewModel workspace)
        {
            Debug.Assert(this.Workspaces.Contains(workspace));
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Workspaces);
            if (collectionView != null)
            {
                collectionView.MoveCurrentTo(workspace);
            }
        }

        #endregion // Private Helpers
    }
}
