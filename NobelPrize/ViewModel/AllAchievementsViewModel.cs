﻿using NobelPrizeWPF.DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace NobelPrizeWPF.ViewModel
{
    class AllAchievementsViewModel : WorkspaceViewModel
    {
        #region Поля
        readonly DatabaseWorker database;
        private Workspace workspace;
        private DataGenerator dataGenerator;
        private List<AchievementViewModel> achievements;
        bool isSelectedAchievement;
        private AchievementViewModel achievementSelected;
        private RelayCommand generateCommand;
        private RelayCommand editCommand;
        private RelayCommand removeCommand;
        #endregion

        public AllAchievementsViewModel(Workspace workspaces, DatabaseWorker db)
        {
            base.DisplayName = "Все открытия";
            workspace = workspaces;
            database = db;
            database.AchievementAdded += OnAchievementAddedToRepository;
            database.AchievementRemoved += OnAchievementRemovedToRepository;
            dataGenerator = new DataGenerator(database);
            CreateAllAchivements();
        }

        #region Свойства отображения
        public ObservableCollection<AchievementViewModel> AllAchievements { get; private set; }

        string removalCheck;
        public string RemovalCheck
        {
            get { return removalCheck; }
            set
            {
                removalCheck = value;
                base.OnPropertyChanged("RemovalCheck");
            }
        }

        #endregion

        #region Публичные методы
        /// <summary>
        /// Команда для генерации поиска открытия по описанию.
        /// </summary>
        public string Searсh
        {
            set
            {
                AllAchievements.Clear();
                foreach (AchievementViewModel vm in achievements)
                {
                    if (vm.Description.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        AllAchievements.Add(vm);
                    }
                }
                base.OnPropertyChanged("AllAchievements");
            }
        }

        /// <summary>
        /// Команда для генерации открытия в базу данных.
        /// </summary>
        public ICommand GenerateCommand
        {
            get
            {
                if (generateCommand == null)
                {
                    generateCommand = new RelayCommand(param => Generate());
                }
                return generateCommand;
            }
        }
        /// <summary>
        /// Генерирует открытие. Этот метод вызывается командой GenerateCommand.
        /// </summary>
        private void Generate()
        {
            dataGenerator.GenerateAchievement();
            base.OnPropertyChanged("AllAchievements");
        }

        /// <summary>
        /// Команда для измения открытия в базе данных.
        /// </summary>
        public ICommand EditCommand
        {
            get
            {
                if (editCommand == null)
                {
                    editCommand = new RelayCommand(param => Edit(), param => isSelectedAchievement);
                }
                return editCommand;
            }
        }
        /// <summary>
        /// Обновляет открытие. Этот метод вызывается командой SaveCommand.
        /// </summary>
        private void Edit()
        {
            workspace.AddWorkspace(achievementSelected);
        }

        /// <summary>
        /// Команда для удаления открытия из базы данных.
        /// </summary>
        public ICommand RemoveCommand
        {
            get
            {
                if (removeCommand == null)
                {
                    removeCommand = new RelayCommand(param => Remove(), param => CanRemove);
                }
                return removeCommand;
            }
        }

        /// <summary>
        /// Удаляет открытие из базы данных. Этот метод вызывается командой RemoveCommand.
        /// </summary>
        public void Remove()
        {
            database.RemoveAchievement(achievementSelected.Achievement);
            isSelectedAchievement = false;
            base.OnPropertyChanged("AllAchievements");
        }

        /// <summary>
        /// Возвращает true, если открытие выделено и может быть удалено.
        /// </summary>
        bool CanRemove
        {
            get { return isSelectedAchievement && achievementSelected.IsRemovable; }
        }

        #endregion // Public Methods

        #region Приватные методы
        private void CreateAllAchivements()
        {
            achievements = (from achievement in database.GetAchievements() select new AchievementViewModel(achievement, database)).ToList();
            foreach (AchievementViewModel vm in achievements)
            {
                vm.PropertyChanged += OnAchievementViewModelPropertyChanged;
            }
            AllAchievements = new ObservableCollection<AchievementViewModel>(achievements);
            AllAchievements.CollectionChanged += this.OnCollectionChanged;
        }
        #endregion

        #region Обработчики событий
        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
            {
                foreach (AchievementViewModel vm in e.NewItems)
                {
                    vm.PropertyChanged += OnAchievementViewModelPropertyChanged;
                }
            }

            if (e.OldItems != null && e.OldItems.Count != 0)
            {
                foreach (AchievementViewModel vm in e.OldItems)
                {
                    vm.PropertyChanged -= OnAchievementViewModelPropertyChanged;
                }
            }
        }

        void OnAchievementViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string IsSelected = "IsSelected";
            (sender as AchievementViewModel).VerifyPropertyName(IsSelected);
            if (e.PropertyName == IsSelected)
            {
                achievementSelected = (AchievementViewModel)sender;
                isSelectedAchievement = true;
                RemoveValid();
            }
        }

        void RemoveValid()
        {
            if (database.CanRemove(achievementSelected.Achievement))
            {
                RemovalCheck = "Открытие не может быть удалено, так как связано с другим элементом";
            }
            else
            {
                RemovalCheck = string.Empty;
            }
        }

        void OnAchievementAddedToRepository(object sender, AchievementEventArgs e)
        {
            var viewModel = new AchievementViewModel(e.achievement, database);
            AllAchievements.Add(viewModel);
        }

        void OnAchievementRemovedToRepository(object sender, AchievementEventArgs e)
        {
            var viewModel = AllAchievements.FirstOrDefault(rem => rem.Achievement == e.achievement);
            AllAchievements.Remove(viewModel);
        }
        #endregion
    }
}
