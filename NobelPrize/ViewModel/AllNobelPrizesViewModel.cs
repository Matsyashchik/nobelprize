﻿using NobelPrizeWPF.DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace NobelPrizeWPF.ViewModel
{
    class AllNobelPrizesViewModel : WorkspaceViewModel
    {
        #region Поля
        readonly DatabaseWorker database;
        private Workspace workspace;
        private DataGenerator dataGenerator;
        private List<NobelPrizeViewModel> nobelPrizes;
        bool isSelectedNobelPrize;
        private NobelPrizeViewModel nobelPrizeSelected;
        private RelayCommand generateCommand;
        private RelayCommand editCommand;
        private RelayCommand removeCommand;
        #endregion

        public AllNobelPrizesViewModel(Workspace workspaces, DatabaseWorker db)
        {
            base.DisplayName = "Все нобелевские премии";
            workspace = workspaces;
            database = db;
            database.NobelPrizeAdded += OnNobelPrizeAddedToRepository;
            database.NobelPrizeRemoved += OnNobelPrizeRemovedToRepository;
            dataGenerator = new DataGenerator(database);
            CreateAllNobelPrizes();
        }

        #region Свойства отображения
        public ObservableCollection<NobelPrizeViewModel> AllNobelPrizes { get; private set; }

        #endregion

        #region Публичные методы
        /// <summary>
        /// Команда для генерации поиска лауреата по имени.
        /// </summary>
        public string Searсh
        {
            set
            {
                AllNobelPrizes.Clear();
                foreach (NobelPrizeViewModel vm in nobelPrizes)
                {
                    if (vm.Laureate.Name.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0 
                        || vm.Achievement.Description.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        AllNobelPrizes.Add(vm);
                    }
                }
                base.OnPropertyChanged("AllNobelPrizes");
            }
        }

        /// <summary>
        /// Команда для генерации открытия в базу данных.
        /// </summary>
        public ICommand GenerateCommand
        {
            get
            {
                if (generateCommand == null)
                {
                    generateCommand = new RelayCommand(param => Generate());
                }
                return generateCommand;
            }
        }

        /// <summary>
        /// Генерирует открытие. Этот метод вызывается командой GenerateCommand.
        /// </summary>
        private void Generate()
        {
            dataGenerator.GenerateNobelPrize();
            base.OnPropertyChanged("AllNobelPrizes");
        }

        /// <summary>
        /// Команда для измения открытия в базе данных.
        /// </summary>
        public ICommand EditCommand
        {
            get
            {
                if (editCommand == null)
                {
                    editCommand = new RelayCommand(param => Edit(), param => isSelectedNobelPrize);
                }
                return editCommand;
            }
        }
        /// <summary>
        /// Обновляет открытие. Этот метод вызывается командой SaveCommand.
        /// </summary>
        private void Edit()
        {
            workspace.AddWorkspace(nobelPrizeSelected);
        }

        /// <summary>
        /// Команда для удаления открытия из базы данных.
        /// </summary>
        public ICommand RemoveCommand
        {
            get
            {
                if (removeCommand == null)
                {
                    removeCommand = new RelayCommand(param => Remove(), param => isSelectedNobelPrize);
                }
                return removeCommand;
            }
        }

        /// <summary>
        /// Удаляет открытие из базы данных. Этот метод вызывается командой RemoveCommand.
        /// </summary>
        public void Remove()
        {
            database.RemoveNobelPrize(nobelPrizeSelected.NobelPrize);
            isSelectedNobelPrize = false;
            base.OnPropertyChanged("AllNobelPrizes");
        }
        #endregion // Public Methods

        #region Приватные методы
        private void CreateAllNobelPrizes()
        {
            nobelPrizes = (from nobelPrize in database.GetNobelPrizes() select new NobelPrizeViewModel(nobelPrize, database)).ToList();
            foreach (NobelPrizeViewModel vm in nobelPrizes)
            {
                vm.PropertyChanged += OnNobelPrizeViewModelPropertyChanged;
            }
            AllNobelPrizes = new ObservableCollection<NobelPrizeViewModel>(nobelPrizes);
            AllNobelPrizes.CollectionChanged += this.OnCollectionChanged;
        }
        #endregion

        #region Обработчики событий
        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
            {
                foreach (NobelPrizeViewModel vm in e.NewItems)
                {
                    vm.PropertyChanged += OnNobelPrizeViewModelPropertyChanged;
                }
            }

            if (e.OldItems != null && e.OldItems.Count != 0)
            {
                foreach (NobelPrizeViewModel vm in e.OldItems)
                {
                    vm.PropertyChanged -= OnNobelPrizeViewModelPropertyChanged;
                }
            }
        }

        void OnNobelPrizeViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string IsSelected = "IsSelected";
            (sender as NobelPrizeViewModel).VerifyPropertyName(IsSelected);
            if (e.PropertyName == IsSelected)
            {
                nobelPrizeSelected = (NobelPrizeViewModel)sender;
                isSelectedNobelPrize = true;
            }
        }

        void OnNobelPrizeAddedToRepository(object sender, NobelPrizeEventArgs e)
        {
            var viewModel = new NobelPrizeViewModel(e.nobelPrize, database);
            AllNobelPrizes.Add(viewModel);
        }

        void OnNobelPrizeRemovedToRepository(object sender, NobelPrizeEventArgs e)
        {
            var viewModel = AllNobelPrizes.FirstOrDefault(rem => rem.NobelPrize == e.nobelPrize);
            AllNobelPrizes.Remove(viewModel);
        }
        #endregion
    }
}