﻿using NobelPrizeWPF.DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NobelPrizeWPF.ViewModel
{
    class AllLaureatesViewModel : WorkspaceViewModel
    {
        #region Поля
        readonly DatabaseWorker database;
        private Workspace workspace;
        private DataGenerator dataGenerator;
        private List<LaureateViewModel> laureates;
        bool isSelectedLaureate;
        private LaureateViewModel laureateSelected;
        private RelayCommand generateCommand;
        private RelayCommand editCommand;
        private RelayCommand removeCommand;
        #endregion

        public AllLaureatesViewModel(Workspace workspaces, DatabaseWorker db)
        {
            base.DisplayName = "Все лауреаты";
            workspace = workspaces;
            database = db;
            database.LaureateAdded += OnLaureateAddedToRepository;
            database.LaureateRemoved += OnLaureateRemovedToRepository;
            dataGenerator = new DataGenerator(database);
            CreateAllLaureates();
        }

        #region Свойства отображения
        public ObservableCollection<LaureateViewModel> AllLaureates { get; private set; }

        string removalCheck;
        public string RemovalCheck
        {
            get { return removalCheck; }
            set
            {
                removalCheck = value;
                base.OnPropertyChanged("RemovalCheck");
            }
        }

        #endregion

        #region Публичные методы
        /// <summary>
        /// Команда для генерации поиска лауреата по имени.
        /// </summary>
        public string Searсh
        {
            set
            {
                AllLaureates.Clear();
                foreach (LaureateViewModel vm in laureates)
                {
                    if (vm.Name.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        AllLaureates.Add(vm);
                    }
                }
                base.OnPropertyChanged("AllLaureates");
            }
        }

        /// <summary>
        /// Команда для генерации открытия в базу данных.
        /// </summary>
        public ICommand GenerateCommand
        {
            get
            {
                if (generateCommand == null)
                {
                    generateCommand = new RelayCommand(param => Generate());
                }
                return generateCommand;
            }
        }
        /// <summary>
        /// Генерирует открытие. Этот метод вызывается командой GenerateCommand.
        /// </summary>
        private void Generate()
        {
            dataGenerator.GenerateLaureate();
            base.OnPropertyChanged("AllLaureates");
        }

        /// <summary>
        /// Команда для измения открытия в базе данных.
        /// </summary>
        public ICommand EditCommand
        {
            get
            {
                if (editCommand == null)
                {
                    editCommand = new RelayCommand(param => Edit(), param => isSelectedLaureate);
                }
                return editCommand;
            }
        }
        /// <summary>
        /// Обновляет открытие. Этот метод вызывается командой SaveCommand.
        /// </summary>
        private void Edit()
        {
            workspace.AddWorkspace(laureateSelected);
        }

        /// <summary>
        /// Команда для удаления открытия из базы данных.
        /// </summary>
        public ICommand RemoveCommand
        {
            get
            {
                if (removeCommand == null)
                {
                    removeCommand = new RelayCommand(param => Remove(), param => CanRemove);
                }
                return removeCommand;
            }
        }

        /// <summary>
        /// Удаляет открытие из базы данных. Этот метод вызывается командой RemoveCommand.
        /// </summary>
        public void Remove()
        {
            database.RemoveLaureate(laureateSelected.Laureate);
            isSelectedLaureate = false;
            base.OnPropertyChanged("AllLaureates");
        }

        /// <summary>
        /// Возвращает true, если открытие выделено и может быть удалено.
        /// </summary>
        bool CanRemove
        {
            get { return isSelectedLaureate && laureateSelected.IsRemovable; }
        }

        #endregion // Public Methods

        #region Приватные методы
        private void CreateAllLaureates()
        {
            laureates = (from Laureate in database.GetLaureates() select new LaureateViewModel(Laureate, database)).ToList();
            foreach (LaureateViewModel vm in laureates)
            {
                vm.PropertyChanged += OnLaureateViewModelPropertyChanged;
            }
            AllLaureates = new ObservableCollection<LaureateViewModel>(laureates);
            AllLaureates.CollectionChanged += this.OnCollectionChanged;
        }
        #endregion

        #region Обработчики событий
        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
            {
                foreach (LaureateViewModel vm in e.NewItems)
                {
                    vm.PropertyChanged += OnLaureateViewModelPropertyChanged;
                }
            }

            if (e.OldItems != null && e.OldItems.Count != 0)
            {
                foreach (LaureateViewModel vm in e.OldItems)
                {
                    vm.PropertyChanged -= OnLaureateViewModelPropertyChanged;
                }
            }
        }

        void OnLaureateViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string IsSelected = "IsSelected";
            (sender as LaureateViewModel).VerifyPropertyName(IsSelected);
            if (e.PropertyName == IsSelected)
            {
                laureateSelected = (LaureateViewModel)sender;
                isSelectedLaureate = true;
                RemoveValid();
            }
        }

        void RemoveValid()
        {
            if (database.CanRemove(laureateSelected.Laureate))
            {
                RemovalCheck = "Лауреат не может быть удалён, так как связан с другим элементом";
            }
            else
            {
                RemovalCheck = string.Empty;
            }
        }

        void OnLaureateAddedToRepository(object sender, LaureateEventArgs e)
        {
            var viewModel = new LaureateViewModel(e.laureate, database);
            AllLaureates.Add(viewModel);
        }

        void OnLaureateRemovedToRepository(object sender, LaureateEventArgs e)
        {
            var viewModel = AllLaureates.FirstOrDefault(rem => rem.Laureate == e.laureate);
            AllLaureates.Remove(viewModel);
        }
        #endregion
    }
}
