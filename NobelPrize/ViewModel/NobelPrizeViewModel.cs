﻿using NobelPrizeWPF.DataAccess;
using NobelPrizeWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

namespace NobelPrizeWPF.ViewModel
{
    class NobelPrizeViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        #region Поля
        readonly DatabaseWorker database;
        private bool isNew;
        private bool isSelected;
        private RelayCommand saveCommand;
        #endregion

        public NobelPrizeViewModel(NobelPrize nobelPrize, DatabaseWorker db)
        {
            isNew = nobelPrize.ID == 0;
            NobelPrize = nobelPrize;
            database = db;
        }

        #region Свойства
        public override string DisplayName
        {
            get
            {
                if (isNew)
                {
                    return "Новая нобелевская премия";
                }
                else
                {
                    return "Автор открытия: " + NobelPrize.Laureate;
                }
            }
        }
        public NobelPrize NobelPrize { get; private set; }

        #region Лауреат
        private List<Laureate> allLaureates;

        public List<Laureate> AllLaureates
        {
            get
            {
                if (allLaureates == null)
                {
                    allLaureates = database.GetLaureates();
                }
                return allLaureates;
            }
        }

        public Laureate Laureate
        {
            get { return NobelPrize.Laureate; }
            set
            {
                if (value == NobelPrize.Laureate)
                {
                    return;
                }
                NobelPrize.NewLaureate(value);
                base.OnPropertyChanged("Laureate");
            }
        }

        public int LaureateIndex
        {
            get
            {
                if (NobelPrize.Laureate != null)
                {
                    for (int i = 0; i < allLaureates.Count; i++)
                    {
                        if (allLaureates[i].ID == NobelPrize.Laureate.ID)
                        {
                            return i;
                        }
                    }
                }
                return -1;
            }
        }
        #endregion

        #region Открытие
        private List<Achievement> allAchievements;

        public List<Achievement> AllAchievements
        {
            get
            {
                if (allAchievements == null)
                {
                    allAchievements = database.GetAchievements();
                }
                return allAchievements;
            }
        }

        public Achievement Achievement
        {
            get { return NobelPrize.Achievement; }
            set
            {
                if (value == NobelPrize.Achievement)
                {
                    return;
                }
                NobelPrize.NewAchievement(value);
                base.OnPropertyChanged("Achievement");
            }
        }

        public int AchievementIndex
        {
            get
            {
                if (NobelPrize.Achievement != null)
                {
                    for (int i = 0; i < allAchievements.Count; i++)
                    {
                        if (allAchievements[i].ID == NobelPrize.Achievement.ID)
                        {
                            return i;
                        }
                    }
                }
                return -1;
            }
        }
        #endregion

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value == isSelected)
                    return;
                isSelected = value;
                base.OnPropertyChanged("IsSelected");
            }
        }
        #endregion

        #region Публичные методы
        /// <summary>
        /// Команда для сохранения открытия в базу данных.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(param => Save(), param => NobelPrize.IsValid);
                }
                return saveCommand;
            }
        }
        /// <summary>
        /// Сохраняет открытие в базу данных. Этот метод вызывается командой SaveCommand.
        /// </summary>
        void Save()
        {
            if (!NobelPrize.IsValid)
            {
                throw new InvalidOperationException("Недопустимый объект");
            }
            if (isNew)
            {
                database.AddNobelPrize(NobelPrize);
                isNew = false;
            }
            else
            {
                database.UpdateNobelPrize(NobelPrize);
            }
            base.OnPropertyChanged("DisplayName");
        }
        #endregion

        #region IDataError
        string IDataErrorInfo.Error
        {
            get { return (NobelPrize as IDataErrorInfo).Error; }
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                string error;
                error = (NobelPrize as IDataErrorInfo)[propertyName];
                CommandManager.InvalidateRequerySuggested();
                return error;
            }
        }
        #endregion
    }
}
