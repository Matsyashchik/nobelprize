﻿using NobelPrizeWPF.DataAccess;
using NobelPrizeWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NobelPrizeWPF.ViewModel
{
    public class AchievementViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        #region Поля
        readonly DatabaseWorker database;
        private bool isNew;
        private bool isSelected;
        private RelayCommand saveCommand;
        #endregion

        public AchievementViewModel(Achievement achievement, DatabaseWorker db)
        {
            isNew = achievement.Category == null;
            Achievement = achievement;
            database = db;
        }

        #region Свойства
        public override string DisplayName
        {
            get
            {
                if (isNew)
                {
                    return "Новое открытие";
                }
                else
                {
                    return Achievement.ToString();
                }
            }
        }

        public Achievement Achievement { get; private set; }
        public List<Category> allCategories { get; private set; }

        public List<Category> AllCategories
        {
            get
            {
                if (allCategories == null)
                {
                    allCategories = new List<Category>();
                    allCategories = database.GetCategories();
                }
                return allCategories;
            }
        }

        public short Year
        {
            get { return Achievement.Year; }
            set
            {
                if (value == Achievement.Year)
                {
                    return;
                }
                Achievement.NewYear(value);
                base.OnPropertyChanged("Year");
            }
        }

        public Category Category
        {
            get { return Achievement.Category; }
            set
            {
                if (value == Achievement.Category)
                {
                    return;
                }
                Achievement.NewCategory(value);
                base.OnPropertyChanged("Category");
            }
        }
        public int CategoryIndex
        {
            get
            {
                if (Achievement.Category != null)
                {
                    for (int i = 0; i < allCategories.Count; i++)
                    {
                        if (allCategories[i].Name == Achievement.Category.Name)
                        {
                            return i;
                        }
                    }
                }
                return -1;
            }
        }

        public string Description
        {
            get { return Achievement.Description; }
            set
            {
                if (value == Achievement.Description)
                {
                    return;
                }
                Achievement.NewDescription(value);
                base.OnPropertyChanged("Description");
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value == isSelected)
                    return;
                isSelected = value;
                base.OnPropertyChanged("IsSelected");
            }
        }

        public bool IsRemovable
        {
            get { return database.CanRemove(Achievement); }
        }

        #endregion

        #region Публичные методы
        /// <summary>
        /// Команда для сохранения открытия в базу данных.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(param => Save(), param => Achievement.IsValid);
                }
                return saveCommand;
            }
        }
        /// <summary>
        /// Сохраняет открытие в базу данных. Этот метод вызывается командой SaveCommand.
        /// </summary>
        void Save()
        {
            if (!Achievement.IsValid)
            {
                throw new InvalidOperationException("Недопустимый объект");
            }
            if (isNew)
            {
                database.AddAchievement(Achievement);
                isNew = false;
            }
            else
            {
                database.UpdateAchievement(Achievement);
            }
            base.OnPropertyChanged("DisplayName");
        }

        #endregion

        #region IDataError
        string IDataErrorInfo.Error
        {
            get { return (Achievement as IDataErrorInfo).Error; }
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                string error;
                error = (Achievement as IDataErrorInfo)[propertyName];
                CommandManager.InvalidateRequerySuggested();
                return error;
            }
        }
        #endregion
    }
}
