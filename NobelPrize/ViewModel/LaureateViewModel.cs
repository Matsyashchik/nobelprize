﻿using NobelPrizeWPF.DataAccess;
using NobelPrizeWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

namespace NobelPrizeWPF.ViewModel
{
    class LaureateViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        #region Поля
        readonly DatabaseWorker database;
        private bool isNew;
        private bool isSelected;
        private RelayCommand saveCommand;
        #endregion

        public LaureateViewModel(Laureate laureate, DatabaseWorker db)
        {
            isNew = laureate.Country == null;
            Laureate = laureate;
            database = db;
        }

        #region Свойства
        public override string DisplayName
        {
            get
            {
                if (isNew)
                {
                    return "Новый лауреат";
                }
                else
                {
                    return Laureate.Name;
                }
            }
        }
        public Laureate Laureate { get; private set; }

        private List<Country> allCountries; 

        public List<Country> AllCountries
        {
            get
            {
                if (allCountries == null)
                {
                    allCountries = database.GetCountries();
                }
                return allCountries;
            }
        }

        public Country Country
        {
            get { return Laureate.Country; }
            set
            {
                if (value == Laureate.Country)
                {
                    return;
                }
                Laureate.NewCountry(value);
                base.OnPropertyChanged("Country");
            }
        }

        public int CountryIndex
        {
            get
            {
                if (Laureate.Country != null)
                {
                    for (int i = 0; i < allCountries.Count; i++)
                    {
                        if (allCountries[i].Name == Laureate.Country.Name)
                        {
                            return i;
                        }
                    }
                }
                return -1;
            }
        }

        public string Name
        {
            get { return Laureate.Name; }
            set
            {
                if (value == Laureate.Name)
                {
                    return;
                }
                Laureate.NewName(value);
                base.OnPropertyChanged("Name");
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value == isSelected)
                    return;
                isSelected = value;
                base.OnPropertyChanged("IsSelected");
            }
        }

        public bool IsRemovable
        {
            get { return database.CanRemove(Laureate); }
        }
        #endregion

        #region Публичные методы
        /// <summary>
        /// Команда для сохранения открытия в базу данных.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(param => Save(), param => Laureate.IsValid);
                }
                return saveCommand;
            }
        }
        /// <summary>
        /// Сохраняет открытие в базу данных. Этот метод вызывается командой SaveCommand.
        /// </summary>
        void Save()
        {
            if (!Laureate.IsValid)
            {
                throw new InvalidOperationException("Недопустимый объект");
            }
            if (isNew)
            {
                database.AddLaureate(Laureate);
                isNew = false;
            }
            else
            {
                database.UpdateLaureate(Laureate);
            }
            base.OnPropertyChanged("DisplayName");
        }
        #endregion

        #region IDataError
        string IDataErrorInfo.Error
        {
            get { return (Laureate as IDataErrorInfo).Error; }
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                string error;
                error = (Laureate as IDataErrorInfo)[propertyName];
                CommandManager.InvalidateRequerySuggested();
                return error;
            }
        }
        #endregion
    }
}
