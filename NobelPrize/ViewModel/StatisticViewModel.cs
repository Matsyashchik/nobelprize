﻿using NobelPrizeWPF.DataAccess;
using NobelPrizeWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NobelPrizeWPF.ViewModel
{
    class StatisticViewModel : WorkspaceViewModel
    {
        readonly DatabaseWorker database;

        public StatisticViewModel(DatabaseWorker db)
        {
            database = db;
            CreateStatistic();
            SetTimeRange();
            StartTime = DateTime.Now.Year;
        }

        #region Свойства отображения
        public override string DisplayName
        {
            get
            {
                int timeRange = (DateTime.Now.Year - StartTime + 1);
                string displayName = "Статистика за " + timeRange;
                int ending = timeRange % 20;
                if (ending < 11)
                {
                    switch (ending)
                    {
                        case 1:
                            displayName += " год";
                            break;
                        case 2:
                        case 3:
                        case 4:
                            displayName += " года";
                            break;
                        default:
                            displayName += " лет";
                            break;
                    }
                }
                else
                {
                    displayName += " лет";
                }
                return displayName;
            }
        }
        private List<Category> Categories { get; set; }
        public ObservableCollection<ObservableCollection<Statistic>> Statistics { get; private set; }
        public ObservableCollection<Statistic> StatisticLaureates { get; private set; }
        public ObservableCollection<Statistic> StatisticAchievements { get; private set; }
        public List<int> TimeRange { get; private set; }
        private int startTime;
        public int StartTime
        {
            get
            {
                return startTime;
            }
            set
            {
                startTime = value;
                ClearStatistic();
                SetStatisitc(value);
                base.OnPropertyChanged("DisplayName");
            }
        }

        #endregion

        #region Приватные методы
        const int firstNobelPrize = 1901;
        private void SetTimeRange()
        {
            TimeRange = new List<int>();
            for (int i = DateTime.Now.Year; i >= firstNobelPrize; i--)
            {
                TimeRange.Add(i);
            }
        }

        private void CreateStatistic()
        {
            Categories = database.GetCategories();
            Categories.Add(new Category(0, ""));
            Statistics = new ObservableCollection<ObservableCollection<Statistic>>();
            for (int i = 0; i < Categories.Count; i++)
            {
                Statistics.Add(new ObservableCollection<Statistic>());
            }
            StatisticLaureates = new ObservableCollection<Statistic>();
            StatisticAchievements = new ObservableCollection<Statistic>();
        }

        private async void SetStatisitc(int range)
        {
            for (int i = 0; i < Categories.Count; i++)
            {
                foreach (Statistic item in await database.LoadStatisticCounrties(range, Categories[i].Name))
                {
                    Statistics[i].Add(item);
                }
            }
            foreach (Statistic item in await database.LoadStatisticLaureates(range))
            {
                StatisticLaureates.Add(item);
            }
            foreach (Statistic item in await database.LoadStatisticAchievements(range))
            {
                StatisticAchievements.Add(item);
            }
        }

        private void ClearStatistic()
        {
            for (int i = 0; i < Categories.Count; i++)
            {
                Statistics[i].Clear();
            }
            StatisticLaureates.Clear();
            StatisticAchievements.Clear();
        }

        #endregion
    }
}
