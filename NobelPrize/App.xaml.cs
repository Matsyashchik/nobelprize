﻿using System;
using System.Windows;

namespace NobelPrizeWPF
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            MainWindow window = new MainWindow();
            string path = @"Data Source = MSI; Initial Catalog = Nobel_Prize; Integrated Security = SSPI";
            var viewModel = new MainWindowViewModel(path);
            EventHandler handler = null;
            handler = delegate
            {
                viewModel.RequestClose -= handler;
                window.Close();
            };
            viewModel.RequestClose += handler;
            window.DataContext = viewModel;
            window.Show();
        }
    }
}