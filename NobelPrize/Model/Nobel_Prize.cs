﻿using System.ComponentModel;

namespace NobelPrizeWPF.Model
{
    public class NobelPrize : IDataErrorInfo
    {
        public int ID { get; private set; }
        public Achievement Achievement { get; private set; }
        public Laureate Laureate { get; private set; }


        public NobelPrize() { }
        public NobelPrize(int id, Achievement achievement, Laureate laureate)
        {
            ID = id;
            Achievement = achievement;
            Laureate = laureate;
        }

        public void NewID(int newID)
        {
            ID = newID;
        }

        public void NewAchievement(Achievement achievement)
        {
            Achievement = achievement;
        }

        public void NewLaureate(Laureate laureate)
        {
            Laureate = laureate;
        }

        string IDataErrorInfo.Error { get { return null; } }

        string IDataErrorInfo.this[string propertyName]
        {
            get { return GetValidationError(propertyName); }
        }

        public bool IsValid
        {
            get
            {
                string[] ValidatedProperties = { "Achievement", "Laureate" };
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)
            {
                case "Achievement":
                    error = ValidateAchievement();
                    break;

                case "Laureate":
                    error = ValidateLaureate();
                    break;
            }
            return error;
        }

        string ValidateAchievement()
        {
            if (Achievement == null)
            {
                return "Открытие не выбрано";
            }
            return null;
        }

        string ValidateLaureate()
        {
            if (Laureate == null)
            {
                return "Лауреат не выбран";
            }
            return null;
        }
    }
}
