﻿namespace NobelPrizeWPF.Model
{
    public class Statistic
    {
        public string Name { get; private set; }
        public int Amount { get; private set; }

        public Statistic(string name, int amount)
        {
            Name = name;
            Amount = amount;
        }
    }
}
