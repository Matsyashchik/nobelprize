﻿namespace NobelPrizeWPF.Model
{
    public class Category
    {
        public byte ID { get; private set; }
        public string Name { get; private set; }
        public Category(byte id, string name)
        {
            ID = id;
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
