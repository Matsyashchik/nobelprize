﻿using System.ComponentModel;

namespace NobelPrizeWPF.Model
{
    public class Laureate : IDataErrorInfo
    {
        public int ID { get; private set; }
        public string Name { get; private set; }
        public Country Country { get; private set; }
        
        public Laureate() { }
        public Laureate(int id, string name, Country country)
        {
            ID = id;
            Name = name;
            Country = country;
        }

        public void NewID(int newID)
        {
            ID = newID;
        }
        public void NewName(string newName)
        {
            Name = newName;
        }
        public void NewCountry(Country newCountry)
        {
            Country = newCountry;
        }
        string IDataErrorInfo.Error { get { return null; } }

        string IDataErrorInfo.this[string propertyName]
        {
            get { return GetValidationError(propertyName); }
        }

        public bool IsValid
        {
            get
            {
                string[] ValidatedProperties = { "Name", "Country" };
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)
            {
                case "Name":
                    error = ValidateName();
                    break;
                case "Country":
                    error = ValidateCountry();
                    break;
            }
            return error;
        }

        string ValidateName()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                return "Имя лауреата отсутствует";
            }
            if (Name.Length > 60)
            {
                return "Поле 'Имя' не должно содержать больше 60 символов";
            }
            return null;
        }

        string ValidateCountry()
        {
            if (Country == null)
            {
                return "Страна не выбрана";
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            Laureate laureate = obj as Laureate;
            if (laureate != null)
            {
                return ID == laureate.ID && Name == laureate.Name;
            }
            return false;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
