﻿using System;
using System.ComponentModel;

namespace NobelPrizeWPF.Model
{
    public class Achievement : IDataErrorInfo
    {
        public int ID { get; private set; }
        public short Year { get; private set; }
        public Category Category { get; private set; }
        public string Description { get; private set; }

        public Achievement() { }
        public Achievement(int id, short year, Category category, string description)
        {
            ID = id;
            Year = year;
            Category = category;
            Description = description;
        }

        public void NewID(int newID)
        {
            ID = newID;
        }

        public void NewYear(short newYear)
        {
            Year = newYear;
        }

        public void NewCategory(Category newCategory)
        {
            Category = newCategory;
        }

        public void NewDescription(string newDescription)
        {
            Description = newDescription;
        }

        string IDataErrorInfo.Error { get { return null; } }

        string IDataErrorInfo.this[string propertyName]
        {
            get { return GetValidationError(propertyName); }
        }

        public bool IsValid
        {
            get
            {
                string[] ValidatedProperties = { "Year", "Category", "Description" };
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)
            {
                case "Year":
                    error = ValidateYear();
                    break;
                case "Category":
                    error = ValidateCategory();
                    break;
                case "Description":
                    error = ValidateDescription();
                    break;
            }
            return error;
        }

        string ValidateYear()
        {
            if (Year < 1901)
            {
                return "Поле 'Год' не может быть раньше 1901 года";
            }
            if (Year > DateTime.Now.Year)
            {
                return "Поле 'Год' не может быть позже текущего года";
            }
            return null;
        }

        string ValidateCategory()
        {
            if (Category == null)
            {
                return "Область науки не выбрана";
            }
            return null;
        }

        string ValidateDescription()
        {
            if (string.IsNullOrWhiteSpace(Description))
            {
                return "Описание отсутствует";
            }
            if (Description.Length > 140)
            {
                return "Поле 'Описание' не должно содержать более 140 символов";
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            Achievement achievement = obj as Achievement;
            if (achievement != null)
            {
                return ID == achievement.ID &&
                    Year == achievement.Year &&
                    Description == achievement.Description;
            }
            return false;
        }

        public override string ToString()
        {
            string description = Description;
            if (description.Length > 40)
            {
                int p = description.LastIndexOf(' ', 37);
                description = p > 0 ? description.Substring(0, p) : description.Substring(0, 37);
                description += "...";
            }
            return description;
        }
    }
}
