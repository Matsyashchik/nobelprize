﻿namespace NobelPrizeWPF.Model
{
    public class Country
    {
        public byte ID { get; private set; }
        public string Name { get; private set; }
        public Country(byte id, string name)
        {
            ID = id;
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
