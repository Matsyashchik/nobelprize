﻿using NobelPrizeWPF.ViewModel;
using System;
using System.Collections.ObjectModel;

namespace NobelPrizeWPF.DataAccess
{
    class Workspace
    {
        public ObservableCollection<WorkspaceViewModel> workspaces;
        public Workspace() { }
        public event EventHandler<WorkspaceEventArgs> WorkspaceEvent;
        internal void AddWorkspace(WorkspaceViewModel inputWorkspaceViewModel)
        {
            if (inputWorkspaceViewModel == null)
            {
                throw new ArgumentNullException("workspace");
            }
            workspaces.Add(inputWorkspaceViewModel);
            if (WorkspaceEvent != null)
            {
                WorkspaceEvent(this, new WorkspaceEventArgs(inputWorkspaceViewModel));
            }
        }
    }
}
