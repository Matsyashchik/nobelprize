﻿using NobelPrizeWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace NobelPrizeWPF.DataAccess
{
    public class DatabaseWorker
    {
        string connectionStr;
        public event EventHandler<ErrorEventArgs> ErrorMessage;

        public DatabaseWorker(string connectionString)
        {
            connectionStr = connectionString;
            LoadDatabase();
        }

        void NoConnectionToDataBase(Exception ex)
        {
            ErrorMessage(this, new ErrorEventArgs("Ошибка при работе с базой данных, " + ex.Message));
        }

        private async void LoadDatabase()
        {
            try
            {
                await LoadAchievements();
                await LoadCategories();
                await LoadLaureates();
                await LoadCountries();
                await LoadNobelPrizes();
            }
            catch (Exception exception)
            {
                NoConnectionToDataBase(exception);
            }
        }

        #region Открытие
        readonly List<Achievement> achievements = new List<Achievement>();
        public event EventHandler<AchievementEventArgs> AchievementAdded;
        public event EventHandler<AchievementEventArgs> AchievementRemoved;

        async Task LoadAchievements()
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string query = "SELECT * FROM Achievements JOIN Categories ON Achievements.id_Category = Categories.ID";
                SqlCommand command = new SqlCommand(query, connection);
                await connection.OpenAsync();
                SqlDataReader reader = await command.ExecuteReaderAsync();
                while (reader.Read())
                {
                    Achievement achievement = new Achievement(
                                id: (int)reader[0],
                                year: (short)reader[1],
                                category: new Category((byte)reader[4], (string)reader[5]),
                                description: (string)reader[3]
                            );
                    achievements.Add(achievement);
                    AchievementAdded?.Invoke(this, new AchievementEventArgs(achievement));
                }
                reader.Close();
                connection.Close();
            }
        }
        public List<Achievement> GetAchievements()
        {
            return achievements;
        }

        public async void AddAchievement(Achievement achievement)
        {
            if (achievement == null)
            {
                throw new ArgumentNullException("Add achievement");
            }

            if (!achievements.Contains(achievement))
            {
                try
                {
                    await AddAchievementToDB(achievement);
                    achievements.Add(achievement);
                    AchievementAdded?.Invoke(this, new AchievementEventArgs(achievement));
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
            }
        }

        async Task AddAchievementToDB(Achievement newAchievement)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "INSERT INTO dbo.Achievements VALUES (@Year, @id_Category, @Description)";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Year", newAchievement.Year);
                command.Parameters.AddWithValue("@id_Category", newAchievement.Category.ID);
                command.Parameters.AddWithValue("@Description", newAchievement.Description);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                command.CommandText = "SELECT @@IDENTITY";
                newAchievement.NewID(Convert.ToInt32(command.ExecuteScalar()));
                connection.Close();
            }
        }

        public async void UpdateAchievement(Achievement achievement)
        {
            if (achievement == null)
            {
                throw new ArgumentNullException("Add achievement");
            }
            if (achievements.Contains(achievement))
            {
                try
                {
                    await UpdateAchievementToDB(achievement);
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
                AchievementAdded?.Invoke(this, new AchievementEventArgs(achievement));
            }

        }

        async Task UpdateAchievementToDB(Achievement newAchievement)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "UPDATE dbo.Achievements SET Year = @Year, id_Category = @id_Category, Description = @Description  WHERE ID = @ID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Year", newAchievement.Year);
                command.Parameters.AddWithValue("@id_Category", newAchievement.Category.ID);
                command.Parameters.AddWithValue("@Description", newAchievement.Description);
                command.Parameters.AddWithValue("@ID", newAchievement.ID);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                connection.Close();
            }
        }

        public async void RemoveAchievement(Achievement removeAchievement)
        {
            if (removeAchievement == null)
            {
                throw new ArgumentNullException("Remove achievement");
            }
            if (achievements.Contains(removeAchievement))
            {
                try
                {
                    await RemoveAchievementFromDB(removeAchievement);
                    achievements.Remove(removeAchievement);
                    AchievementRemoved?.Invoke(this, new AchievementEventArgs(removeAchievement));
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
            }
        }

        async Task RemoveAchievementFromDB(Achievement achievement)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "DELETE dbo.Achievements WHERE ID = @ID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ID", achievement.ID);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                connection.Close();
            }
        }

        public bool CanRemove(Achievement achievement)
        {
            if (achievements.Contains(achievement))
            {
                foreach (NobelPrize nobelPrize in nobelPrizes)
                {
                    if (nobelPrize.Achievement.Equals(achievement))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        #endregion

        #region Область науки
        readonly List<Category> categories = new List<Category>();
        public List<Category> GetCategories()
        {
            return categories;
        }

        async Task LoadCategories()
        {
            try
            {
                string query = "SELECT * FROM Categories";
                SqlConnection connection = new SqlConnection(connectionStr);
                SqlCommand command = new SqlCommand(query, connection);
                using (connection)
                {
                    await connection.OpenAsync();
                    SqlDataReader reader = await command.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        categories.Add(new Category(id: (byte)reader[0], name: (string)reader[1]));
                    }
                    reader.Close();
                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                NoConnectionToDataBase(exception);
            }
        }
        #endregion

        #region Лауреат
        readonly List<Laureate> laureates = new List<Laureate>();
        public event EventHandler<LaureateEventArgs> LaureateAdded;
        public event EventHandler<LaureateEventArgs> LaureateRemoved;

        async Task LoadLaureates()
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string query = "SELECT * FROM Laureates JOIN Countries ON Laureates.id_Country = Countries.ID";
                SqlCommand command = new SqlCommand(query, connection);
                await connection.OpenAsync();
                SqlDataReader reader = await command.ExecuteReaderAsync();
                while (reader.Read())
                {
                    Laureate laureate = new Laureate(
                                id: (int)reader[0],
                                name: (string)reader[1],
                                country: new Country((byte)reader[3], (string)reader[4])
                            );
                    laureates.Add(laureate);
                    LaureateAdded?.Invoke(this, new LaureateEventArgs(laureate));
                }
                reader.Close();
                connection.Close();
            }
        }

        public List<Laureate> GetLaureates()
        {
            return laureates;
        }

        public async void AddLaureate(Laureate laureate)
        {
            if (laureate == null)
            {
                throw new ArgumentNullException("Add laureate");
            }
            if (!laureates.Contains(laureate))
            {
                try
                {
                    await AddLaureateToDB(laureate);
                    laureates.Add(laureate);
                    LaureateAdded?.Invoke(this, new LaureateEventArgs(laureate));
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
            }
        }

        async Task AddLaureateToDB(Laureate newLaureate)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "INSERT INTO dbo.Laureates VALUES (@Name, @id_Country)";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Name", newLaureate.Name);
                command.Parameters.AddWithValue("@id_Country", newLaureate.Country.ID);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                command.CommandText = "SELECT @@IDENTITY";
                newLaureate.NewID(Convert.ToInt32(command.ExecuteScalar()));
                connection.Close();
            }
        }

        public async void UpdateLaureate(Laureate laureate)
        {
            if (laureate == null)
            {
                throw new ArgumentNullException("Add laureate");
            }
            if (laureates.Contains(laureate))
            {
                try
                {
                    await UpdateLaureateToDB(laureate);
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
                LaureateAdded?.Invoke(this, new LaureateEventArgs(laureate));
            }

        }

        async Task UpdateLaureateToDB(Laureate newLaureate)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "UPDATE dbo.Laureates SET Name = @Name, id_Country = @id_Country WHERE ID = @ID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Name", newLaureate.Name);
                command.Parameters.AddWithValue("@id_Country", newLaureate.Country.ID);
                command.Parameters.AddWithValue("@ID", newLaureate.ID);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                connection.Close();
            }
        }

        public async void RemoveLaureate(Laureate removeLaureate)
        {
            if (removeLaureate == null)
            {
                throw new ArgumentNullException("Remove laureate");
            }
            if (laureates.Contains(removeLaureate))
            {
                try
                {
                    await RemoveLaureateFromDB(removeLaureate);
                    laureates.Remove(removeLaureate);
                    LaureateRemoved?.Invoke(this, new LaureateEventArgs(removeLaureate));
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
            }
        }

        async Task RemoveLaureateFromDB(Laureate laureate)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "DELETE dbo.Laureates WHERE ID = @ID";
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlParameter IdParametr = new SqlParameter("@ID", laureate.ID);
                command.Parameters.Add(IdParametr);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                connection.Close();
            }
        }

        public bool CanRemove(Laureate laureate)
        {
            if (laureates.Contains(laureate))
            {
                foreach (NobelPrize nobelPrize in nobelPrizes)
                {
                    if (nobelPrize.Laureate.Equals(laureate))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion

        #region Страна
        readonly List<Country> countries = new List<Country>();
        public List<Country> GetCountries()
        {
            return countries;
        }

        async Task LoadCountries()
        {
            try
            {
                string query = "SELECT * FROM Countries";
                SqlConnection connection = new SqlConnection(connectionStr);
                SqlCommand command = new SqlCommand(query, connection);
                using (connection)
                {
                    await connection.OpenAsync();
                    SqlDataReader reader = await command.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        countries.Add(new Country(id: (byte)reader[0], name: (string)reader[1]));
                    }
                    reader.Close();
                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                NoConnectionToDataBase(exception);
            }
        }
        #endregion

        #region Нобелевская премия
        readonly List<NobelPrize> nobelPrizes = new List<NobelPrize>();
        public event EventHandler<NobelPrizeEventArgs> NobelPrizeAdded;
        public event EventHandler<NobelPrizeEventArgs> NobelPrizeRemoved;

        async Task LoadNobelPrizes()
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string query = "SELECT * FROM NobelPrizes";
                SqlCommand command = new SqlCommand(query, connection);
                await connection.OpenAsync();
                SqlDataReader reader = await command.ExecuteReaderAsync();
                while (reader.Read())
                {
                    NobelPrize nobelPrize = new NobelPrize(
                                id: (int)reader[0],
                                laureate: new Laureate(
                                    id: (int)reader[1],
                                    name: (string)reader[2],
                                    country: new Country(
                                        id: (byte)reader[3],
                                        name: (string)reader[4])
                                    ),
                                achievement: new Achievement(
                                    id: (int)reader[5],
                                    year: (short)reader[6],
                                    description: (string)reader[7],
                                    category: new Category(
                                        id: (byte)reader[8],
                                        name: (string)reader[9])
                                    )
                            );
                    nobelPrizes.Add(nobelPrize);
                    NobelPrizeAdded?.Invoke(this, new NobelPrizeEventArgs(nobelPrize));
                }
                reader.Close();
                connection.Close();
            }
        }

        public List<NobelPrize> GetNobelPrizes()
        {
            return nobelPrizes;
        }

        public async void AddNobelPrize(NobelPrize newNobelPrize)
        {
            if (newNobelPrize == null)
            {
                throw new ArgumentNullException("Add NobelPrize");
            }
            if (!nobelPrizes.Contains(newNobelPrize))
            {
                try
                {
                    await AddNobelPrizeToDB(newNobelPrize);
                    nobelPrizes.Add(newNobelPrize);
                    NobelPrizeAdded?.Invoke(this, new NobelPrizeEventArgs(newNobelPrize));
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
            }
        }

        async Task AddNobelPrizeToDB(NobelPrize newNobelPrizes)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "INSERT INTO dbo.Nobel_Prizes VALUES (@id_Laureate, @id_Achievement)";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id_Laureate", newNobelPrizes.Laureate.ID);
                command.Parameters.AddWithValue("@id_Achievement", newNobelPrizes.Achievement.ID);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                command.CommandText = "SELECT @@IDENTITY";
                newNobelPrizes.NewID(Convert.ToInt32(command.ExecuteScalar()));
                connection.Close();
            }
        }

        public async void UpdateNobelPrize(NobelPrize nobelPrize)
        {
            if (nobelPrize == null)
            {
                throw new ArgumentNullException("Add laureate");
            }
            if (nobelPrizes.Contains(nobelPrize))
            {
                try
                {
                    await UpdateNobelPrizeToDB(nobelPrize);
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
                NobelPrizeAdded?.Invoke(this, new NobelPrizeEventArgs(nobelPrize));
            }

        }

        async Task UpdateNobelPrizeToDB(NobelPrize nobelPrizes)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "UPDATE dbo.Nobel_Prizes SET id_Laureate = @id_Laureate, id_Achievement = @id_Achievement WHERE ID = @ID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id_Laureate", nobelPrizes.Laureate.ID);
                command.Parameters.AddWithValue("@id_Achievement", nobelPrizes.Achievement.ID);
                command.Parameters.AddWithValue("@ID", nobelPrizes.ID);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                connection.Close();
            }
        }

        public async void RemoveNobelPrize(NobelPrize removeNobelPrize)
        {
            if (removeNobelPrize == null)
            {
                throw new ArgumentNullException("Remove NobelPrize");
            }
            if (nobelPrizes.Contains(removeNobelPrize))
            {
                try
                {
                    await RemoveNobelPrizeFromDB(removeNobelPrize);
                    nobelPrizes.Remove(removeNobelPrize);
                    NobelPrizeRemoved?.Invoke(this, new NobelPrizeEventArgs(removeNobelPrize));
                }
                catch (Exception ex)
                {
                    NoConnectionToDataBase(ex);
                }
            }
        }

        async Task RemoveNobelPrizeFromDB(NobelPrize nobelPrize)
        {
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                string queryString = "DELETE dbo.Nobel_Prizes WHERE ID = @ID";
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlParameter IdParametr = new SqlParameter("@ID", nobelPrize.ID);
                command.Parameters.Add(IdParametr);
                await connection.OpenAsync();
                SqlDataReader DR = await command.ExecuteReaderAsync();
                DR.Close();
                connection.Close();
            }
        }
        #endregion

        #region Статистика
        public async Task<List<Statistic>> LoadStatisticCounrties(int year, string category)
        {
            string query = "SELECT Country, COUNT(id_Country) FROM NobelPrizes WHERE Year > @Year AND Category LIKE @Category " +
                "GROUP BY id_Country, Country ORDER BY COUNT(id_Country) DESC";
            
            return await LoadStatisticInfo(query, year, category);
        }

        public async Task<List<Statistic>> LoadStatisticLaureates(int year)
        {
            string query = "SELECT Name, COUNT(id_Achievement) FROM NobelPrizes WHERE Year >= @Year " +
                "GROUP BY id_Laureate, Name HAVING COUNT(id_Achievement) > 1 ORDER BY COUNT(id_Achievement) DESC";
            return await LoadStatisticInfo(query, year);
        }

        public async Task<List<Statistic>> LoadStatisticAchievements(int year)
        {
            string query = "SELECT Description, COUNT(id_Laureate) FROM NobelPrizes WHERE Year >= @Year " +
                "GROUP BY id_Achievement, Description HAVING COUNT(id_Laureate) > 1 ORDER BY COUNT(id_Laureate) DESC ";
            return await LoadStatisticInfo(query, year);
        }

        private async Task<List<Statistic>> LoadStatisticInfo(string query, int year, string category = null)
        {
            List<Statistic> statisticCounrties = new List<Statistic>();
            SqlConnection connection = new SqlConnection(connectionStr);
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@Year", year);
            command.Parameters.AddWithValue("@Category", $"%{category}");
            try
            {
                using (connection)
                {
                    await connection.OpenAsync();
                    SqlDataReader reader = await command.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        Statistic statistic = new Statistic(name: (string)reader[0], amount: (int)reader[1]);
                        statisticCounrties.Add(statistic);
                    }
                    reader.Close();
                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                NoConnectionToDataBase(exception);
            }
            return statisticCounrties;
        }
        #endregion
    }
}