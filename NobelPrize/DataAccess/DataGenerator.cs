﻿using NobelPrizeWPF.Model;
using System;
using System.Collections.Generic;

namespace NobelPrizeWPF.DataAccess
{
    public class DataGenerator
    {
        DatabaseWorker database;
        Random random = new Random();
        List<Category> categories;
        List<Country> countries;
        List<Achievement> achievements;
        List<Laureate> laureates;
        private string[] descriptions = { 
            "Синтез органических красителей, гидроароматических соединений",
            "За свою роль в подписании Портсмутского договора",
            "Исследование электропроводимости газов",
            "Работы по гистологии и морфологии нервной системы",
            "За свою неустанную деятельность во имя мира",
            "Открытие болезнетворной роли простейших",
            "Работы по исследованию механизмов иммунитета",
            "Работы в области беспроволочного телеграфа",
            "За организацию конференций по разоружению",
            "Как истинный лидер народного движения за мир в Европе",
            "Исследования уравнения состояния газов и жидкостей",
            "Открытие и исследование анафилаксии",
            "Основополагающие работы в области химических координационных (комплексных) соединений",
        };
        private string[] names = {
            "Анри Беккерель",
            "Стретт Рэлей",
            "Альберт Абрахам Майкельсон",
            "Карл Фердинанд Браун",
            "Ван-дер-Ваальс",
            "Уильям Генри Брэгг",
            "Уильям Лоренс Брэгг",
            "Нильс Бор",
            "Жан Батист Перрен",
            "Мюррей Гелл-Манн",
            "Леон Купер",
            "Абдуса Салама",
            "Кай Сигбана",
            "Макс Карл Эрнст Людвиг Планк",
            "Перси Уильямс Бриджмен",
            "Эрнест Лоуренс",
            "Отто Штерн",
            "Исидор Айзек Раби",
            "Вольфганг Паули",
            "Перси Уильямс Бриджмен",
            "Феликс Блох",
            "Лев Ландау",
            "Мо Янь",
        };
        public DataGenerator(DatabaseWorker database)
        {
            categories = database.GetCategories();
            countries = database.GetCountries();
            achievements = database.GetAchievements();
            laureates = database.GetLaureates();
            this.database = database;
        }

        public Achievement GenerateAchievement()
        {
            Achievement newAchievement = new Achievement();
            newAchievement.NewYear(Convert.ToInt16(random.Next(1901, DateTime.Now.Year)));
            newAchievement.NewCategory(categories[random.Next(categories.Count)]);
            newAchievement.NewDescription(descriptions[random.Next(descriptions.Length)]);
            database.AddAchievement(newAchievement);
            return newAchievement;
        }

        public Laureate GenerateLaureate()
        {
            Laureate newLaureate = new Laureate();
            newLaureate.NewName(names[random.Next(names.Length)]);
            newLaureate.NewCountry(countries[random.Next(countries.Count)]);
            database.AddLaureate(newLaureate);
            return newLaureate;
        }

        public NobelPrize GenerateNobelPrize()
        {
            NobelPrize newNobelPrize = new NobelPrize();
            newNobelPrize.NewAchievement(achievements[random.Next(achievements.Count)]);
            newNobelPrize.NewLaureate(laureates[random.Next(laureates.Count)]);
            database.AddNobelPrize(newNobelPrize);
            return newNobelPrize;
        }
    }
}
