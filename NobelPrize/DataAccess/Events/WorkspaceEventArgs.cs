﻿using NobelPrizeWPF.ViewModel;

namespace NobelPrizeWPF.DataAccess
{
    class WorkspaceEventArgs
    {
        public WorkspaceEventArgs(WorkspaceViewModel inputWorkspaceViewModel)
        {
            workspaceViewModel = inputWorkspaceViewModel;
        }
        public WorkspaceViewModel workspaceViewModel { get; private set; }
    }
}
