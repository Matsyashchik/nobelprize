﻿using System;

namespace NobelPrizeWPF.DataAccess
{
    public class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs(string errormessage)
        {
            message = errormessage;
        }
        public string message { get; private set; }
    }
}
