﻿using NobelPrizeWPF.Model;

namespace NobelPrizeWPF.DataAccess
{
    public class LaureateEventArgs
    {
        public LaureateEventArgs(Laureate laureate)
        {
            this.laureate = laureate;
        }
        public Laureate laureate { get; private set; }
    }
}
