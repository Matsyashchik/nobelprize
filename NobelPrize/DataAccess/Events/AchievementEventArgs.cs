﻿using NobelPrizeWPF.Model;

namespace NobelPrizeWPF.DataAccess
{
    public class AchievementEventArgs
    {
        public AchievementEventArgs(Achievement achievement)
        {
            this.achievement = achievement;
        }
        public Achievement achievement { get; private set; }
    }
}
