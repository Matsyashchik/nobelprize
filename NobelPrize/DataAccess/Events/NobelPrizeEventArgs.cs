﻿using NobelPrizeWPF.Model;

namespace NobelPrizeWPF.DataAccess
{
    public class NobelPrizeEventArgs
    {
        public NobelPrizeEventArgs(NobelPrize nobelPrize)
        {
            this.nobelPrize = nobelPrize;
        }
        public NobelPrize nobelPrize { get; private set; }
    }
}
