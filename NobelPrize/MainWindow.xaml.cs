﻿using System.Windows;

namespace NobelPrizeWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        void MenuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Cтудент группы БИСТ-311\nМацящик Владислав Феликсович\nCГУПС. 2020", "Разработчик");
        }

        void MenuItemReference_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("База данных хранит данные о лауреатах Нобелевской премии. " +
                "Для каждого лауреата хранятся ФИО, год получения Нобелевской премии, область науки, описание открытия." +
                "\nНобелевская премия в одном году по одной области науки может быть присуждена нескольким учёным.");
        }
    }
}
